package com.appshouse.takaful.utilities;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by Mohammed Algassab on 11/18/2014.
 */
public class CustomJSONObject extends Request<JSONObject> {

    private Response.Listener<JSONObject> listener;
    private Map<String, String> params;
    Context context;

    public CustomJSONObject(int method, String url, Map<String, String> params, Response.Listener<JSONObject>
            reponseListener, Response.ErrorListener errorListener) {
        super(method, url, errorListener);
        this.setRetryPolicy(new DefaultRetryPolicy(20000, 3, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        this.listener = reponseListener;
        this.params = params;
    }


    @Override
    protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {

        String jsonString;
        try {
            jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers));
            return Response.success(new JSONObject(jsonString), HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }


    @Override
    protected void deliverResponse(JSONObject jsonArray) {
        listener.onResponse(jsonArray);
    }
}