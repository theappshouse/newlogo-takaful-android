package com.appshouse.takaful.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appshouse.takaful.R;
import com.appshouse.takaful.attributes.OurCenter;
import com.appshouse.takaful.utilities.MyMethods;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

/**
 * Created by Mohammed Algassab on 1/6/2016.
 */
public class OurCenterAdapter extends RecyclerView.Adapter<OurCenterAdapter.OurCenterViewHolder> {

    private List<OurCenter> ourCenterList;
    private Context context;
    Typeface font;

    public class OurCenterViewHolder extends RecyclerView.ViewHolder implements OnMapReadyCallback {
        MapView mapView;
        GoogleMap map;
        Context context;

        TextView tvName;
        TextView tvType;
        TextView tvTime;
        TextView tvPhone;
        LinearLayout lloNavigate;
        LinearLayout lloPhone;

        public OurCenterViewHolder(View itemView, Context context, Typeface font) {
            super(itemView);
            this.context = context;
            mapView = (MapView) itemView.findViewById(R.id.map);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            tvType = (TextView) itemView.findViewById(R.id.tvType);
            tvTime = (TextView) itemView.findViewById(R.id.tvTime);
            tvPhone = (TextView) itemView.findViewById(R.id.tvPhone);
            lloNavigate = (LinearLayout) itemView.findViewById(R.id.lloNavigate);
            lloPhone = (LinearLayout) itemView.findViewById(R.id.lloPhone);

            if (mapView != null) {
                mapView.onCreate(null);
                mapView.onResume();
                mapView.getMapAsync(this);
            }

            tvName.setTypeface(font);
            tvType.setTypeface(font);
            tvTime.setTypeface(font);
            tvPhone.setTypeface(font);
        }

        @Override
        public void onMapReady(GoogleMap googleMap) {
            Log.i("GoogleMap", "onMapReady");
            MapsInitializer.initialize(context);
            map = googleMap;
            int pos = getPosition();
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(ourCenterList.get(pos).mLocation, 13));
            map.getUiSettings().setMapToolbarEnabled(false);
            map.addMarker(new MarkerOptions().position(ourCenterList.get(pos).mLocation));
        }
    }

    public OurCenterAdapter(Context context, List<OurCenter> ourCenterList) {
        this.ourCenterList = ourCenterList;
        this.context = context;
        font = MyMethods.getRegularFont(context);
    }

    @Override
    public OurCenterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_center, parent, false);
        return new OurCenterViewHolder(rowView, context, font);
    }

    @Override
    public void onBindViewHolder(OurCenterViewHolder holder, int position) {
        final OurCenter ourCenter = ourCenterList.get(position);
        holder.tvName.setText(ourCenter.mName);
        holder.tvType.setText(ourCenter.mType);
        holder.tvTime.setText(ourCenter.mTime);
        holder.tvTime.setTextSize(TypedValue.COMPLEX_UNIT_PX, ourCenter.mTimeTextSize);
        holder.tvPhone.setText(ourCenter.mPhone);
        GoogleMap thisMap = holder.map;
        //then move map to 'location'
        if (thisMap != null) {
            thisMap.getUiSettings().setMapToolbarEnabled(false);
            thisMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ourCenter.mLocation, 13));
            thisMap.addMarker(new MarkerOptions().position(ourCenter.mLocation));
        }

        holder.lloNavigate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyMethods.openGoogleMap(context, String.valueOf(ourCenter.mLocation.latitude),
                        String.valueOf(ourCenter.mLocation.longitude), ourCenter.mName);
            }
        });

        holder.lloPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyMethods.dailCall(context, ourCenter.mPhone);
            }
        });
    }

    @Override
    public void onViewRecycled(OurCenterViewHolder holder) {
        super.onViewRecycled(holder);
        if (holder.map != null) {
            holder.map.clear();
            holder.map.setMapType(GoogleMap.MAP_TYPE_NONE);
        }
    }

    @Override
    public int getItemCount() {
        return ourCenterList.size();
    }
}




