package com.appshouse.takaful.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appshouse.takaful.R;
import com.appshouse.takaful.activities.NewsDetailActivity;
import com.appshouse.takaful.attributes.News;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.utilities.MyMethods;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Mohammed Algassab on 1/31/2016.
 */
public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {

    private List<News> newsList;
    private Context context;


    public static class NewsViewHolder extends RecyclerView.ViewHolder {
        TextView tvNewsTitle;
        ImageView ivNews;
        LinearLayout lloRow;

        public NewsViewHolder(View itemView, Context context) {
            super(itemView);
            tvNewsTitle = (TextView) itemView.findViewById(R.id.tvNewsTitle);
            tvNewsTitle.setTypeface(MyMethods.getRegularFont(context));
            ivNews = (ImageView) itemView.findViewById(R.id.ivNews);
            lloRow = (LinearLayout) itemView.findViewById(R.id.lloRow);
        }
    }

    public NewsAdapter(Context context, List<News> newsList) {
        this.newsList = newsList;
        this.context = context;
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_news, parent, false);
        return new NewsViewHolder(rowView, context);
    }

    @Override
    public void onBindViewHolder(NewsViewHolder holder, int position) {
        final News news = newsList.get(position);
        holder.tvNewsTitle.setText(news.mTitle);
        Picasso.with(context).load(APIs.ImageURL + (news.mImage.replaceAll(" ", "%20")))
                .placeholder(R.drawable.news_sample).into(holder.ivNews);
        holder.lloRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, NewsDetailActivity.class);
                intent.putExtra("news", news);
                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }

}

