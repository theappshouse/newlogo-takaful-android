package com.appshouse.takaful.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.appshouse.takaful.R;
import com.appshouse.takaful.activities.AgencyActivity;
import com.appshouse.takaful.attributes.Directory;
import com.appshouse.takaful.utilities.MyMethods;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Mohammed Algassab on 1/6/2016.
 */
public class DirectoryAdapter extends RecyclerView.Adapter<DirectoryAdapter.DirectoryViewHolder> {

    private List<Directory> directoryList;
    private Context context;
    Typeface font;

    public static class DirectoryViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;
        LinearLayout lloRow;
        ImageView ivDirectory;

        public DirectoryViewHolder(View itemView, Typeface font) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            lloRow = (LinearLayout) itemView.findViewById(R.id.lloRow);
            ivDirectory = (ImageView) itemView.findViewById(R.id.ivDirectory);

            tvTitle.setTypeface(font);
        }
    }

    public DirectoryAdapter(Context context, List<Directory> directoryList) {
        this.directoryList = directoryList;
        this.context = context;
        font = MyMethods.getRegularFont(context);
    }

    @Override
    public DirectoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate
                (R.layout.row_directory, parent, false);
        return new DirectoryViewHolder(rowView, font);
    }

    @Override
    public void onBindViewHolder(DirectoryViewHolder holder, final int position) {
        final Directory directory = directoryList.get(position);
        holder.tvTitle.setText(directory.mTitle);
        Picasso.with(context).load(directory.mImageRes).into(holder.ivDirectory);
        holder.lloRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //position 0 mean agency button
                if (position == 0) {
                    context.startActivity(new Intent(context, AgencyActivity.class));
                } else {
                    MyMethods.openLink(context, directory.mLink);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return directoryList.size();
    }

}




