package com.appshouse.takaful.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.activities.AppController;
import com.appshouse.takaful.adapters.MyTakafulAdapter;
import com.appshouse.takaful.attributes.MyTakaful;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.constant.ProductCodes;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.SharedPreferencesClass;
import com.appshouse.takaful.utilities.WrappingLinearLayoutManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mohammed Algassab on 1/6/2016.
 */
public class MyTakafulFragment extends Fragment {
    View view;
    TextView tvName, tvCpr, tvPhone, tvAddress;
    RecyclerView lvCar, lvTravel, lvFire, lvFireComp;
    List<MyTakaful> carMyTakafulArrayList, travelMyTakafulArrayList, fireMyTakafulArrayList, fireCompMyTakafulArrayList;
    RecyclerView.Adapter carMyTakafulAdapter, travelMyTakafulAdapter, fireMyTakafulAdapter, fireCompMyTakafulAdapter;
    View flCarType, flTravelType, flFireType, flFireCompType;

    SharedPreferencesClass setting;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_my_takaful, container, false);
        tvName = (TextView) view.findViewById(R.id.tvName);
        tvCpr = (TextView) view.findViewById(R.id.tvCpr);
        tvPhone = (TextView) view.findViewById(R.id.tvPhone);
        tvAddress = (TextView) view.findViewById(R.id.tvAddress);

        lvCar = (RecyclerView) view.findViewById(R.id.lvCar);
        lvTravel = (RecyclerView) view.findViewById(R.id.lvTravel);
        lvFire = (RecyclerView) view.findViewById(R.id.lvFire);
        lvFireComp = (RecyclerView) view.findViewById(R.id.lvFireComp);

        flCarType = view.findViewById(R.id.flCarType);
        flTravelType = view.findViewById(R.id.flTravelType);
        flFireType = view.findViewById(R.id.flFireType);
        flFireCompType = view.findViewById(R.id.flFireCompType);

        lvCar.setLayoutManager(new WrappingLinearLayoutManager(getContext()));
        lvTravel.setLayoutManager(new WrappingLinearLayoutManager(getContext()));
        lvFire.setLayoutManager(new WrappingLinearLayoutManager(getContext()));
        lvFireComp.setLayoutManager(new WrappingLinearLayoutManager(getContext()));

        lvCar.setHasFixedSize(true);
        lvTravel.setHasFixedSize(true);
        lvFire.setHasFixedSize(true);
        lvFireComp.setHasFixedSize(true);

        lvCar.setNestedScrollingEnabled(false);
        lvTravel.setNestedScrollingEnabled(false);
        lvFire.setNestedScrollingEnabled(false);
        lvFireComp.setNestedScrollingEnabled(false);
        //lvCar.setLayoutManager(linearLayoutManager);

        setting = new SharedPreferencesClass(getActivity());
        ViewGroup root = (ViewGroup) view.findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(getActivity()));
        /*final AlertDialog ad = new AlertDialog.Builder(getActivity()).create();
        ad.setCancelable(false);
        //ad.setTitle(title);
        //ad.setMessage("We are upgrading our system to provide you with better services. Apologies for any inconvenient this might cause.");

        ad.setMessage(getString(R.string.vatmessage));
        ad.setButton(Dialog.BUTTON_NEUTRAL,"OK",new DialogInterface.OnClickListener(){

            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });


        ad.show();*/

        getMyTakaful();
        return view;
    }

    String Tag_Request = "login";

    private void getMyTakaful() {
        MyMethods.showLoading(view);
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetMyTakaful
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MyMethods.showMainView(view);

                //crash start from this point

                setMyTakaful(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyMethods.showMainView(view);
                MyMethods.showNetworkErrorDialog(getActivity(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getMyTakaful();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("cpr", setting.getCpr());
                params.put("userId", String.valueOf(setting.getUserId()));
                params.put("token", setting.getAccessToken());
                params.put("deviceId", MyMethods.getDeviceId(getActivity()));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void setMyTakaful(JSONObject jsonObject) {

        try {
            Log.i("getUserInfo", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                JSONObject userResult = jsonObject.getJSONObject("result").getJSONObject("userInfo");
                String firstName = userResult.getString("firstName").isEmpty() ? "" : (userResult.getString("firstName").trim() + " ");
                String middleName = userResult.getString("middleName").isEmpty() ? "" : (userResult.getString("middleName").trim() + " ");
                String lastName = userResult.getString("lastName").isEmpty() ? "" : (userResult.getString("lastName").trim() + " ");
                tvName.setText(firstName + middleName + lastName);

                String building;
                if (userResult.getString("flatNo").isEmpty() || userResult.getString("flatNo").contentEquals("null")) {
                    building = "House " + userResult.getString("houseNo");
                } else {
                    building = "Flat " + userResult.getString("flatNo") + ", BLD " + userResult.getString("houseNo");
                }
                tvAddress.setText(building + ", R " + userResult.getString("road") + ", Block " + userResult.getString("block"));

                tvCpr.setText(setting.getCpr());
                tvPhone.setText(userResult.getString("mobileNo"));

                //set policies list
                JSONArray policiesResult = jsonObject.getJSONObject("result").getJSONArray("policiesList");
                carMyTakafulArrayList = new ArrayList<>();
                travelMyTakafulArrayList = new ArrayList<>();
                fireMyTakafulArrayList = new ArrayList<>();
                fireCompMyTakafulArrayList = new ArrayList<>();
                for (int i = 0; i < policiesResult.length(); i++) {
                    JSONObject policyObject = policiesResult.getJSONObject(i);
                    Log.d("policyObject",policyObject.toString()) ;

                    String quoteNo = policyObject.getString("quoteNo");
                    String policyNo = policyObject.getString("policyNumber");
                    String productCode = policyObject.getString("productCode");
                    boolean isRenew = policyObject.getBoolean("renew");
                    boolean isExpired = policyObject.getBoolean("expired");
                    int status = policyObject.getInt("policyStatus");
                    Log.d("policyStatus", String.valueOf(status)) ;
                    String startDate = policyObject.getString("policyStartDate");
                    String expireDate = policyObject.getString("policyExpiryDate");
                    String policyHolderName = policyObject.getString("policyHolderName");
                    String policyInceptionDate = policyObject.getString("policyInceptionDate");
                    String policyGrossPremium = policyObject.getString("policyGrossPremium");
                    String policyNetPremium = policyObject.getString("policyNetPremium");
                    String policySumInsured = policyObject.getString("policySumInsured");
                    String productName = policyObject.getString("productName");

                    if (productCode.contentEquals(ProductCodes.MOTOR)) {
                        carMyTakafulArrayList.add(new MyTakaful(quoteNo, policyNo, isRenew, isExpired, status,
                                productCode, startDate, expireDate, policyHolderName, policyInceptionDate,
                                policyGrossPremium, policyNetPremium, policySumInsured, getString(R.string.product_motor)));
                    } else if (productCode.contentEquals(ProductCodes.TRAVEL)) {
                        travelMyTakafulArrayList.add(new MyTakaful(quoteNo, policyNo, isRenew, isExpired, status,
                                productCode, startDate, expireDate, policyHolderName, policyInceptionDate,
                                policyGrossPremium, policyNetPremium, policySumInsured, getString(R.string.prodcut_travel)));
                    } else if (productCode.contentEquals(ProductCodes.FIRE)) {
                        fireMyTakafulArrayList.add(new MyTakaful(quoteNo, policyNo, isRenew, isExpired, status,
                                productCode, startDate, expireDate, policyHolderName, policyInceptionDate,
                                policyGrossPremium, policyNetPremium, policySumInsured, getString(R.string.product_fire)));
                    } else if (productCode.contentEquals(ProductCodes.BAITAK)) {
                        fireCompMyTakafulArrayList.add(new MyTakaful(quoteNo, policyNo, isRenew, isExpired, status,
                                productCode, startDate, expireDate, policyHolderName, policyInceptionDate,
                                policyGrossPremium, policyNetPremium, policySumInsured, getString(R.string.prodcut_baitak)));
                    }
                }

                if (carMyTakafulArrayList.size() > 0) {
                    flCarType.setVisibility(View.VISIBLE);
                    carMyTakafulAdapter = new MyTakafulAdapter(getActivity(), carMyTakafulArrayList);
                    lvCar.setAdapter(carMyTakafulAdapter);
                }
                if (travelMyTakafulArrayList.size() > 0) {
                    flTravelType.setVisibility(View.VISIBLE);
                    travelMyTakafulAdapter = new MyTakafulAdapter(getActivity(), travelMyTakafulArrayList);
                    lvTravel.setAdapter(travelMyTakafulAdapter);
                }
                if (fireMyTakafulArrayList.size() > 0) {
                    flFireType.setVisibility(View.VISIBLE);
                    fireMyTakafulAdapter = new MyTakafulAdapter(getActivity(), fireMyTakafulArrayList);
                    lvFire.setAdapter(fireMyTakafulAdapter);
                }
                if (fireCompMyTakafulArrayList.size() > 0) {
                    flFireCompType.setVisibility(View.VISIBLE);
                    fireCompMyTakafulAdapter = new MyTakafulAdapter(getActivity(), fireCompMyTakafulArrayList);
                    lvFireComp.setAdapter(fireCompMyTakafulAdapter);
                }
            } else if (jsonObject.getInt("status") == 4) {
                MyMethods.showLoginPage(getActivity());
            } else {
                MyMethods.showNetworkErrorDialog(getActivity(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getMyTakaful();
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        AppController.getInstance().cancelPendingRequests(Tag_Request);
    }

}
