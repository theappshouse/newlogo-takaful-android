package com.appshouse.takaful.fragments;

import android.animation.ValueAnimator;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.activities.AppController;
import com.appshouse.takaful.activities.MainActivity;
import com.appshouse.takaful.activities.MyNoteActivity;
import com.appshouse.takaful.activities.ProductDetailActivity;
import com.appshouse.takaful.attributes.Product;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Mohammed Algassab on 12/31/2015.
 */
public class HomeFragment extends Fragment implements View.OnTouchListener, View.OnClickListener {

    ImageSwitcher isProduct;
    TextSwitcher tsProductTitle, tsProductDesc;
    TextView tvProductTitle, tvProductDescription;
    LinearLayout lloIndicatorHolder;

    ArrayList<Product> productArrayList;
    //Store the images in this list to view them in the slider
    List<Drawable> productImageList;
    int indicatorWidth, indicatorHeight;
    LinearLayout bMyTakaful, bRoadAssistance, bMyClaim, bGetQuote, bProducts, bOurCenter, bSupport,
            bMyNote, bDirectory;
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        ViewGroup root = (ViewGroup) view.findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(getActivity()));

        isProduct = (ImageSwitcher) view.findViewById(R.id.isProduct);
        tvProductTitle = (TextView) view.findViewById(R.id.tvProductTitle);
        tvProductDescription = (TextView) view.findViewById(R.id.tvProductDesc);
        tsProductTitle = (TextSwitcher) view.findViewById(R.id.tsProductTitle);
        tsProductDesc = (TextSwitcher) view.findViewById(R.id.tsProductDesc);
        lloIndicatorHolder = (LinearLayout) view.findViewById(R.id.lloIndicatorHolder);
        bMyTakaful = (LinearLayout) view.findViewById(R.id.bMyTakaful);
        bRoadAssistance = (LinearLayout) view.findViewById(R.id.bRoadAssistant);
        bMyClaim = (LinearLayout) view.findViewById(R.id.bMyClaims);
        bGetQuote = (LinearLayout) view.findViewById(R.id.bGetQuote);
        bProducts = (LinearLayout) view.findViewById(R.id.bProducts);
        bOurCenter = (LinearLayout) view.findViewById(R.id.bOurCenter);
        bSupport = (LinearLayout) view.findViewById(R.id.bSupport);
        bMyNote = (LinearLayout) view.findViewById(R.id.bMyNote);
        bDirectory = (LinearLayout) view.findViewById(R.id.bDirectory);

        //Store the width and height of the indicator to start animation depend on these sizes
        indicatorWidth = getResources().getDimensionPixelOffset(R.dimen.width_homeIndicator);
        indicatorHeight = getResources().getDimensionPixelOffset(R.dimen.height_homeIndicatorSelected);

        bMyTakaful.setOnClickListener(this);
        bRoadAssistance.setOnClickListener(this);
        bMyClaim.setOnClickListener(this);
        bGetQuote.setOnClickListener(this);
        bProducts.setOnClickListener(this);
        bOurCenter.setOnClickListener(this);
        bSupport.setOnClickListener(this);
        bMyNote.setOnClickListener(this);
        bDirectory.setOnClickListener(this);

        getFeaturedProduct();
        return view;
    }


    String Tag_Request = "get_product_featured";

    private void getFeaturedProduct() {
        Log.i("getFeaturedProduct","start get product");
        view.findViewById(R.id.lyLoading).setVisibility(View.VISIBLE);
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetProducts
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                setFeaturedProduct(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyMethods.showNetworkErrorDialog(getActivity(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getFeaturedProduct();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("isFeature", "1");
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void setFeaturedProduct(JSONObject jsonObject) {
        try {
            Log.i("setProductQuote", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                productArrayList = new ArrayList<>();
                JSONArray resultArray = jsonObject.getJSONArray("result");

                for (int i = 0; i < resultArray.length(); i++) {
                    JSONObject productObject = resultArray.getJSONObject(i);
                    Product product = new Product(
                            productObject.getInt("Id"),
                            productObject.getString("Product_Code"),
                            productObject.getString(getString(R.string.z_product_name)),
                            productObject.getString(getString(R.string.z_short_description)),
                            productObject.getString(getString(R.string.z_product_details)),
                            productObject.getString("Product_Image"),
                            productObject.getString("Product_Thumbnail"),
                            productObject.getInt("Is_Feature") == 1,
                            productObject.getInt("Has_Quote") == 1);
                    productArrayList.add(product);
                }
                setupImageSlider();
                catchImage();
            } else {
                Snackbar.make(view, R.string.failed_load_featured_product, Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                getFeaturedProduct();
                            }
                        }).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //Define the properties of the image slider and the text that will be changed over time.
    private void setupImageSlider() {
        Animation in = AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_right);
        Animation in2 = AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in);
        Animation out = AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_out);
        isProduct.setInAnimation(in2);
        isProduct.setOutAnimation(out);
        tsProductTitle.setInAnimation(in2);
        tsProductTitle.setOutAnimation(out);
        tsProductDesc.setInAnimation(in2);
        tsProductDesc.setOutAnimation(out);

        isProduct.setFactory(new ViewSwitcher.ViewFactory() {
            public View makeView() {
                ImageView imageView = new ImageView(getActivity());
                imageView.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT
                        , FrameLayout.LayoutParams.MATCH_PARENT));
                imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                return imageView;
            }
        });

        tsProductTitle.setFactory(new ViewSwitcher.ViewFactory() {
            public View makeView() {
                TextView myText = new TextView(getActivity());
                myText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.textSize_homeSliderTitle));
                myText.setTextColor(Color.WHITE);

                //TODO: Remove it when implementing rtl
                myText.setGravity(Gravity.LEFT);

                myText.setTypeface(MyMethods.getBoldFont(getActivity()));
                return myText;
            }
        });

        tsProductDesc.setFactory(new ViewSwitcher.ViewFactory() {
            public View makeView() {
                TextView myText = new TextView(getActivity());
                myText.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.textSize_homeSliderSubTitle));
                myText.setTextColor(Color.WHITE);

                //TODO: Remove it when implementing rtl
                myText.setGravity(Gravity.LEFT);

                myText.setSingleLine(true);
                myText.setTypeface(MyMethods.getRegularFont(getActivity()));
                return myText;
            }
        });
    }

    private void catchImage() {
        productImageList = new ArrayList<>();
        loadBitmap(APIs.ImageURL + productArrayList.get(sliderIndex).mImagePath);
    }


    private Target loadTarget;

    public void loadBitmap(String url) {
        if (loadTarget == null)
            loadTarget = new Target() {
                @Override
                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                    //isAdded to check if the fragment attached to activity
                    if (isAdded()) {
                        productImageList.add(new BitmapDrawable(getResources(), bitmap));
                        sliderIndex++;
                        if (sliderIndex == productArrayList.size()) {
                            sliderIndex = 0;
                            view.findViewById(R.id.lyLoading).setVisibility(View.GONE);
                            if (productArrayList.size() > 1) {
                                setIndicators();
                                runSlider();

                                //To be able to change slider by gesture
                                isProduct.setOnTouchListener(HomeFragment.this);
                            } else {
                                //If only single product we just set the data once and do not run the slider
                                tvProductTitle.setText(productArrayList.get(sliderIndex).mTitle);
                                tvProductDescription.setText(productArrayList.get(sliderIndex).mShotDesc);
                                isProduct.setImageDrawable(productImageList.get(sliderIndex));
                            }
                        } else {
                            loadBitmap(APIs.ImageURL + productArrayList.get(sliderIndex).mImagePath);
                        }
                    }
                }

                @Override
                public void onBitmapFailed(Drawable drawable) {
                }

                @Override
                public void onPrepareLoad(Drawable drawable) {
                }
            };
        Picasso.with(getActivity()).load(url).into(loadTarget);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (sliderRunner != null)
            sliderHandler.removeCallbacks(sliderRunner);

        AppController.getInstance().cancelPendingRequests(Tag_Request);
    }

    //Remove previous indicators and create indicator depend on products number
    List<View> indicatorList;

    private void setIndicators() {
        //The user may preform refresh function so we need to remove the indicators before adding to avoid repeated indicators
        if (indicatorList != null)
            lloIndicatorHolder.removeAllViews();

        indicatorList = new ArrayList<>();
        for (int i = 0; i < productArrayList.size(); i++) {
            LayoutInflater.from(getActivity()).inflate(R.layout.view_indicator, lloIndicatorHolder);
            View indicator = lloIndicatorHolder.getChildAt(i);
            indicatorList.add(indicator);
        }
    }

    Runnable sliderRunner;
    Handler sliderHandler;
    int sliderIndex = 0;
    int sliderDelay = 7000;

    //Run the slider by setting the first data in TextView and ImageView then switch the data
    //every sliderDelay time and depend on sliderIndex.
    private void runSlider() {
        isProduct.setImageDrawable(productImageList.get(sliderIndex));
        tsProductTitle.setText(productArrayList.get(sliderIndex).mTitle);
        tsProductDesc.setText(productArrayList.get(sliderIndex).mShotDesc);
        moveIndicator(sliderIndex);

        sliderRunner = new Runnable() {
            @Override
            public void run() {
                sliderIndex++;

                //Change the slider information depend in sliderIndex position
                moveSlider();
                sliderHandler.postDelayed(sliderRunner, sliderDelay);
            }
        };
        sliderHandler = new Handler();
        sliderHandler.postDelayed(sliderRunner, sliderDelay);


    }

    private void moveSlider() {
        if (sliderIndex == productArrayList.size())
            sliderIndex = 0;
        else if (sliderIndex < 0)
            sliderIndex = productArrayList.size() - 1;

        isProduct.setImageDrawable(productImageList.get(sliderIndex));
        tsProductTitle.setText(productArrayList.get(sliderIndex).mTitle);
        tsProductDesc.setText(productArrayList.get(sliderIndex).mShotDesc);

        //after moving the slider we also move the indicator
        moveIndicator(sliderIndex);
    }

    //We move the indicator by performing some animation that change the indicator view size.
    int previousPosition = 0;

    private void moveIndicator(final int position) {
        //we increase the height of the new indicator with animation
        ValueAnimator valueAnimator = ValueAnimator.ofInt(indicatorWidth, indicatorHeight);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                ViewGroup.LayoutParams layoutParams = indicatorList.get(position).getLayoutParams();
                indicatorList.get(position).getLayoutParams().height = (Integer) valueAnimator.getAnimatedValue();
                indicatorList.get(position).setLayoutParams(layoutParams);
            }
        });

        //We decrease the height of the previous indicator and return it to it normal shape
        indicatorList.get(previousPosition).getLayoutParams().height = indicatorWidth;
        previousPosition = position;
        valueAnimator.start();
    }

    //The below is for changing the slider when the user hover the slider to the left or right
    float initialX;

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                initialX = event.getX();
                break;
            case MotionEvent.ACTION_UP:
                float finalX = event.getX();
                if (initialX > finalX && (initialX - finalX) > 100) {
                    sliderIndex++;
                    moveSlider();
                } else if (finalX > initialX && (finalX - initialX) > 100) {
                    sliderIndex--;
                    moveSlider();
                } else {
                    Intent intent = new Intent(getActivity(), ProductDetailActivity.class);
                    intent.putExtra("product", productArrayList.get(sliderIndex));
                    startActivity(intent);
                }
                sliderHandler.removeCallbacks(sliderRunner);
                sliderHandler.postDelayed(sliderRunner, sliderDelay);
                break;
        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bMyTakaful:
                if (MyMethods.isLogin(getActivity(), view))
                    ((MainActivity) getActivity()).replaceFragment(new MyTakafulFragment(), getString(R.string.sm_myTakaful), false);
                break;

            case R.id.bRoadAssistant:
                if (MyMethods.isLogin(getActivity(), view))
                    ((MainActivity) getActivity()).replaceFragment(new RoadAssistanceFragment(), getString(R.string.sm_roadAssistance), false);
                break;

            case R.id.bMyClaims:
             //   if (MyMethods.isLogin(getActivity(), view))
               //     ((MainActivity) getActivity()).replaceFragment(new ClaimListFragment(), getString(R.string.sm_myClaim), false);
                Snackbar.make(getActivity().findViewById(android.R.id.content),
                        getString(R.string.comingSoon), Snackbar.LENGTH_LONG).show();
                break;

            case R.id.bGetQuote:
                ((MainActivity) getActivity()).replaceFragment(new GetQuoteFragment(), getString(R.string.sm_getQuote), false);
                break;
            case R.id.bProducts:
                ((MainActivity) getActivity()).replaceFragment(new ProductFragment(), getString(R.string.sm_product), false);
                break;

            case R.id.bOurCenter:
                ((MainActivity) getActivity()).replaceFragment(new OurCenterFragment(), getString(R.string.sm_ourCenter), false);
                break;

            case R.id.bSupport:
                ((MainActivity) getActivity()).replaceFragment(new SupportFragment(), getString(R.string.sm_support), false);
                break;

            case R.id.bMyNote:
                ((MainActivity) getActivity()).replaceFragment(new NewsFragment(), getString(R.string.sm_news), false);
                break;

            case R.id.bDirectory:
                ((MainActivity) getActivity()).replaceFragment(new DirectoryFragment(), getString(R.string.sm_directory), false);
                break;
        }
    }
}
