package com.appshouse.takaful.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.activities.AppController;
import com.appshouse.takaful.adapters.AgencyAdapter;
import com.appshouse.takaful.attributes.Agency;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Mohammed Algassab on 3/13/2016.
 */
public class AgencyFragment extends Fragment {

    RecyclerView lvAgency;
    ArrayList<Agency> agencyArrayList;
    RecyclerView.Adapter agencyAdapter;
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_agency, container, false);

        lvAgency = (RecyclerView) view.findViewById(R.id.lvAgency);
        lvAgency.setLayoutManager(new LinearLayoutManager(getActivity()));
        lvAgency.setHasFixedSize(true);
        //setAgencyData();
        getAgency();
        return view;
    }
    String Tag_Request = "get_agency";

    private void getAgency() {
        MyMethods.showLoading(view);
        CustomJSONObject req = new CustomJSONObject(Request.Method.GET, APIs.GetAgency
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MyMethods.showMainView(view);
                setAgencyData(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("VolleyError", error.toString());
                MyMethods.showNetworkErrorDialog(getActivity(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getAgency();
                    }
                });
            }
        });
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void setAgencyData(JSONObject jsonObject) {
        try {
            Log.i("setAgencyData", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                agencyArrayList = new ArrayList<>();
                JSONArray resultArray = jsonObject.getJSONArray("result");

                for (int i = 0; i < resultArray.length(); i++) {
                    JSONObject agencyObject = resultArray.getJSONObject(i);
                    Agency agency = new Agency(agencyObject.getString("Logo"), agencyObject.getString("Car_Name"),
                            agencyObject.getString("Agency_Name"), agencyObject.getString("Address"),
                            agencyObject.getString("Phone"), agencyObject.getString("Showroom_Office_Days"),
                            agencyObject.getString("Showroom_Office_Hours"), agencyObject.getString("Services_Office_Days"),
                            agencyObject.getString("Services_Office_Hours"),agencyObject.getDouble("Latitude"),
                            agencyObject.getDouble("Longitude"));
                    agencyArrayList.add(agency);
                }

                agencyAdapter = new AgencyAdapter(getActivity(), agencyArrayList);
                lvAgency.setAdapter(agencyAdapter);
            } else {
                MyMethods.showNetworkErrorDialog(getActivity(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getAgency();
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

//    private void setAgencyData() {
//        agencyArrayList = new ArrayList<>();
//        agencyArrayList.add(new Agency(R.drawable.ag_acura, "Acura", "National Motor Company W.L.L.",
//                "Building 4443 Road 475 Block 604, Sitra, 11722, Manama, Bahrain", "973 17457100",
//                "Sat - Thu \n8:00 - 7:00", "Sat - Thu\n7:00 - 6:00", 26.101117, 50.361571));
//
//        agencyArrayList.add(new Agency(R.drawable.ag_alfa, "Alfa Romeo", "BEHBEHANI BROTHERS W.L.L P.O. ",
//                "Building 432, Rd No 1211, Riffa", "973 17459811",
//                "Sat to Thu \n8:30 - 12:30", "Sat - Thu  \n7:00 - 4:15", 26.101117, 50.361571));
//
//        agencyArrayList.add(new Agency(R.drawable.ag_aston, "Aston Martin", "Montana Motors",
//                "Building 4443 Road 475 Block 604, Sitra, 11722, Manama, Bahrain", "973 17669999",
//                "Sat - Thu \n8:00 - 7:00", "No time \navailable", 26.101117, 50.361571));
//
//        agencyArrayList.add(new Agency(R.drawable.ag_audi, "Audi", "BEHBEHANI BROTHERS W.L.L ",
//                "Shaikh Jaber A. Al Subah Hwy, Highway No. 792 Sitra", "973 17459933",
//                "Sat - Fri \n8:00 - 7:00", "Sat - Wed \n7:00 - 4:15", 26.101117, 50.361571));
//
//        agencyAdapter = new DirectoryAdapter(getActivity(), agencyArrayList);
//        lvAgency.setAdapter(agencyAdapter);
//    }
}
