package com.appshouse.takaful.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.activities.AppController;
import com.appshouse.takaful.activities.ForgetPasswordActivity;
import com.appshouse.takaful.activities.MainActivity;
import com.appshouse.takaful.activities.RegisterActivity;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.SharedPreferencesClass;
import com.parse.ParsePush;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mohammed Algassab on 1/5/2016.
 */
public class LoginFragment extends Fragment {

    EditText etCpr, etPassword;
    LinearLayout bLogin, bForgetPassword;
    Button bRegister;
    View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_login, container, false);
        bRegister = (Button) view.findViewById(R.id.bRegister);
        bForgetPassword = (LinearLayout) view.findViewById(R.id.bForgetPassword);
        etCpr = (EditText) view.findViewById(R.id.etCpr);
        etPassword = (EditText) view.findViewById(R.id.etPassword);
        bLogin = (LinearLayout) view.findViewById(R.id.bLogin);

        bLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                prepareLogin();
            }
        });

        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), RegisterActivity.class));
            }
        });

        bForgetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ForgetPasswordActivity.class));
            }
        });


        etPassword.setGravity(new SharedPreferencesClass(getActivity()).getLanguage().contentEquals("en") ? Gravity.LEFT : Gravity.RIGHT);
        ViewGroup root = (ViewGroup) view.findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(getActivity()));
        return view;
    }

    String cpr, password;

    private void prepareLogin() {
        MyMethods.hideKeyboard(getActivity());
        cpr = etCpr.getText().toString();
        password = etPassword.getText().toString();

        if (cpr.isEmpty() || password.isEmpty()) {
            Snackbar.make(view, getString(R.string.message_enterRequiredData), Snackbar.LENGTH_LONG).show();
        } else {
            login();
        }
    }

    String Tag_Request = "login";

    private void login() {
        final ProgressDialog mDialog = new ProgressDialog(getActivity());
        mDialog.setMessage(getString(R.string.progress_loading));
        mDialog.show();
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.Login
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mDialog.dismiss();
                getLoginResult(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mDialog.dismiss();
                Snackbar.make(view, R.string.networkError, Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                login();
                            }
                        }).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("cpr", cpr);
                params.put("password", password);
                params.put("deviceId", MyMethods.getDeviceId(getActivity()));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void getLoginResult(JSONObject jsonObject) {
        try {
            Log.i("getSendQuoteResult", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                JSONObject jsonResult = jsonObject.getJSONObject("result");
                Toast.makeText(getActivity(), R.string.login_success, Toast.LENGTH_LONG).show();
                SharedPreferencesClass setting = new SharedPreferencesClass(getActivity());
                setting.setLogin(jsonResult.getInt("Id"), cpr, jsonResult.getString("Email"), jsonResult.getString("token"));
                ParsePush.subscribeInBackground("c" + cpr);

                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            } else {
                MyMethods.showSnackBarMessage(view, jsonObject.getString(getString(R.string.api_message)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        AppController.getInstance().cancelPendingRequests(Tag_Request);
    }
}
