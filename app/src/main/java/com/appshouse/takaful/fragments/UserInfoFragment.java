package com.appshouse.takaful.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.activities.AppController;
import com.appshouse.takaful.activities.MainActivity;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.SharedPreferencesClass;
import com.parse.ParsePush;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mohammed Algassab on 1/5/2016.
 */
public class UserInfoFragment extends Fragment {

    TextView tvName, tvCpr, tvEmail, tvAddress, tvCity, tvPhone;
    View view;
    SharedPreferencesClass setting;
    Button bLogout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_user_info, container, false);
        tvName = (TextView) view.findViewById(R.id.tvName);
        tvCpr = (TextView) view.findViewById(R.id.tvCpr);
        tvEmail = (TextView) view.findViewById(R.id.tvEmail);
        tvAddress = (TextView) view.findViewById(R.id.tvAddress);
        tvPhone = (TextView) view.findViewById(R.id.tvPhone);
        tvCity = (TextView) view.findViewById(R.id.tvCity);
        bLogout = (Button) view.findViewById(R.id.bLogout);

        ViewGroup root = (ViewGroup) view.findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(getActivity()));

        setting = new SharedPreferencesClass(getActivity());
        tvCpr.setText(setting.getCpr());
        tvEmail.setText(setting.getEmail());

        loadUserInfo();

        bLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
        return view;
    }

    String Tag_Request = "userInfo";

    private void loadUserInfo() {
        MyMethods.showLoading(view);
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetUserInfo
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MyMethods.showMainView(view);
                getUserInfo(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyMethods.showMainView(view);
                MyMethods.showNetworkErrorDialog(getActivity(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        loadUserInfo();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("cpr", setting.getCpr());
                params.put("userId", String.valueOf(setting.getUserId()));
                params.put("token", setting.getAccessToken());
                params.put("deviceId", MyMethods.getDeviceId(getActivity()));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }


    private void getUserInfo(JSONObject jsonObject) {
        try {
            Log.i("getUserInfo", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                JSONObject jsonResult = jsonObject.getJSONObject("result");
                String firstName = jsonResult.getString("firstName").isEmpty() ? "" : (jsonResult.getString("firstName").trim() + " ");
                String middleName = jsonResult.getString("middleName").isEmpty() ? "" : (jsonResult.getString("middleName").trim() + " ");
                String lastName = jsonResult.getString("lastName").isEmpty() ? "" : (jsonResult.getString("lastName").trim() + " ");
                tvName.setText(firstName + middleName + lastName);
                tvPhone.setText(jsonResult.getString("mobileNo"));
                tvCity.setText(jsonResult.getString("city"));
                String building;
                if (jsonResult.getString("flatNo").isEmpty() || jsonResult.getString("flatNo").contentEquals("null")) {
                    building = "House " + jsonResult.getString("houseNo");
                } else {
                    building = "Flat " + jsonResult.getString("flatNo") + ", BLD " + jsonResult.getString("houseNo");
                }
                tvAddress.setText(building + ", R " + jsonResult.getString("road") + ", Block " + jsonResult.getString("block"));
            } else if (jsonObject.getInt("status") == 4) {
                MyMethods.showLoginPage(getActivity());
            } else {
                MyMethods.showNetworkErrorDialog(getActivity(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        loadUserInfo();
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    String Tag_Request_Logout = "logout";

    private void logout() {
        MyMethods.showLoading(view);
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.Logout
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ParsePush.unsubscribeInBackground("c" + setting.getCpr());
                Toast.makeText(getActivity(), R.string.logout_success, Toast.LENGTH_LONG).show();
                setting.logout();
                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyMethods.showNetworkErrorDialog(getActivity(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        logout();
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("userId", String.valueOf(setting.getUserId()));
                params.put("deviceId", MyMethods.getDeviceId(getActivity()));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request_Logout);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        AppController.getInstance().cancelPendingRequests(Tag_Request);
        AppController.getInstance().cancelPendingRequests(Tag_Request_Logout);
    }
}
