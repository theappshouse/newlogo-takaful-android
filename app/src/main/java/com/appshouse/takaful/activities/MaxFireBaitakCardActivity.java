package com.appshouse.takaful.activities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.appshouse.takaful.R;
import com.appshouse.takaful.adapters.PagerAdapter;
import com.appshouse.takaful.attributes.MyTakaful;
import com.appshouse.takaful.fragments.TakafulMaxFireBaitakCardFragment;
import com.appshouse.takaful.utilities.MyMethods;

/**
 * Created by Mohammed Algassab on 1/10/2016.
 */
public class MaxFireBaitakCardActivity extends AppCompatActivity {
    ViewPager viewPager;
    ImageView ivMinimize, ivEngIndicator, ivArIndicator, ivSave;
    int WRITE_EXTERNAL = 2;
    MyTakaful myTakaful;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_maximize);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        ivEngIndicator = (ImageView) findViewById(R.id.ivEngIndicator);
        ivArIndicator = (ImageView) findViewById(R.id.ivArIndicator);
        ivMinimize = (ImageView) findViewById(R.id.ivMinimize);
        ivSave = (ImageView) findViewById(R.id.ivSave);
        viewPager.setOffscreenPageLimit(2);
        viewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        ivEngIndicator.setImageResource(R.drawable.shape_card_indicator_selected);
                        ivArIndicator.setImageResource(R.drawable.shape_card_indicator);
                        break;
                    case 1:
                        ivEngIndicator.setImageResource(R.drawable.shape_card_indicator);
                        ivArIndicator.setImageResource(R.drawable.shape_card_indicator_selected);
                        break;
                }
            }
        });

        ivSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(MaxFireBaitakCardActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    MyMethods.rationalDialog(MaxFireBaitakCardActivity.this, R.string.dialog_message_permStorage, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ActivityCompat.requestPermissions(MaxFireBaitakCardActivity.this,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_EXTERNAL);
                        }
                    });
                } else {
                    ActivityCompat.requestPermissions(MaxFireBaitakCardActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, WRITE_EXTERNAL);
                }
            }
        });
        setData();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == WRITE_EXTERNAL) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                ivSave.setVisibility(View.GONE);
                ivMinimize.setVisibility(View.GONE);
                View view = findViewById(R.id.ivCard);
                view.setDrawingCacheEnabled(true);
                Bitmap b = view.getDrawingCache();

                if (MyMethods.saveImageToExternalStorage(this, b, myTakaful.mProductCode + "_" + myTakaful.mPolicyNo + "_" + System.currentTimeMillis())) {
                    Snackbar.make(view, R.string.message_card_saved_success, Snackbar.LENGTH_INDEFINITE).
                            setAction(R.string.dialog_button_default, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                }
                            }).show();
                } else {
                    Snackbar.make(view, R.string.message_card_saved_failed, Snackbar.LENGTH_LONG).show();
                }
                ivSave.setVisibility(View.VISIBLE);
                ivMinimize.setVisibility(View.VISIBLE);
            } else if (!ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                MyMethods.rationalDialog(this, R.string.dialog_message_permStorage, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, 2);
                    }
                });
            }
        }
    }

    private void setData() {
        TakafulMaxFireBaitakCardFragment takafulMaxFireBaitakCardFragment = new TakafulMaxFireBaitakCardFragment();
        myTakaful = getIntent().getParcelableExtra("myTakaful");
        Bundle argument = new Bundle();
        argument.putString("name", myTakaful.mPolicyHolderName);
        argument.putString("firstNumber", myTakaful.mPolicyNo);
        argument.putString("policyStart", myTakaful.mPolicyStart);
        argument.putString("policyEnd", myTakaful.mPolicyEnd);
        argument.putString("cover", myTakaful.mCover);
        argument.putString("sumCover", myTakaful.mSumCover);
        argument.putString("houseNo", myTakaful.mHouseNo);
        argument.putString("blockNo", myTakaful.mBlockNo);
        argument.putString("roadNo", myTakaful.mRoadNo);
        argument.putString("city", myTakaful.mCity);
        takafulMaxFireBaitakCardFragment.setArguments(argument);

        TakafulMaxFireBaitakCardFragment takafulMaxFireBaitakCardFragmentAr = new TakafulMaxFireBaitakCardFragment();
        Bundle argumentAR = new Bundle();
        argumentAR.putBoolean("ar", true);
        argumentAR.putString("name", myTakaful.mPolicyHolderName);
        argumentAR.putString("firstNumber", myTakaful.mPolicyNo);
        argumentAR.putString("policyStart", myTakaful.mPolicyStart);
        argumentAR.putString("policyEnd", myTakaful.mPolicyEnd);
        argumentAR.putString("cover", myTakaful.mCover);
        argumentAR.putString("sumCover", myTakaful.mSumCover);
        argumentAR.putString("houseNo", myTakaful.mHouseNo);
        argumentAR.putString("blockNo", myTakaful.mBlockNo);
        argumentAR.putString("roadNo", myTakaful.mRoadNo);
        argumentAR.putString("city", myTakaful.mCity);
        takafulMaxFireBaitakCardFragmentAr.setArguments(argumentAR);

        PagerAdapter mPagerAdapter = new PagerAdapter(getSupportFragmentManager());
        mPagerAdapter.addFragment(takafulMaxFireBaitakCardFragment);
        mPagerAdapter.addFragment(takafulMaxFireBaitakCardFragmentAr);

        viewPager.setAdapter(mPagerAdapter);
        ivMinimize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
