package com.appshouse.takaful.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.adapters.SpinnerAdapter;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.SharedPreferencesClass;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static android.text.InputType.TYPE_CLASS_TEXT;
import static android.text.InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS;

/**
 * Created by Mohammed Algassab on 2/7/2016.
 */
public class SaveQuoteTravelActivity extends AppCompatActivity {
    Toolbar toolbar;
    double premium;
    String sumAssured;
    TextView tvPremium,tvSumAssured;
    int travelerNo;
    Typeface font;
    LinearLayout lloTravelerField;
    SharedPreferencesClass setting;
    EditText etCpr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_travel_quote);

        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_saveTravelQuote);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        setting = new SharedPreferencesClass(this);

        ViewGroup root = (ViewGroup) findViewById(R.id.main);
        font = MyMethods.getRegularFont(this);
        MyMethods.setFont(root, font);

        tvPremium = (TextView) findViewById(R.id.tvPremium);
        tvSumAssured = (TextView) findViewById(R.id.tvSumAssured);
        etCpr = (EditText) findViewById(R.id.etCpr);
        lloTravelerField = (LinearLayout) findViewById(R.id.lloTravelerFields);

        premium = getIntent().getDoubleExtra("premium", 0);
        sumAssured = getIntent().getStringExtra("sumAssured");
        travelerNo = Integer.parseInt(getIntent().getStringExtra("noTrav"));
        tvPremium.setText(getString(R.string.bd)+String.valueOf(premium));
        tvSumAssured.setText(String.valueOf(sumAssured));

        if (setting.isLogin()) {
            etCpr.setText(setting.getCpr());
            etCpr.setEnabled(false);
        }

        for (int i = 0; i < travelerNo; i++)
            createTravelerFields(i);
    }

    private void createTravelerFields(int id) {
        LinearLayout.LayoutParams lyTextView = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lyTextView.setMargins(15, getResources().getDimensionPixelOffset(R.dimen.marginTop_textViewEditTextTitle), 15, 0);

        LinearLayout.LayoutParams lyEditText = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lyEditText.setMargins(15, getResources().getDimensionPixelOffset(R.dimen.marginTop_defaultEditText), 15, 0);

        LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setTag("traveler" + id);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setLayoutParams(lyTextView);

        TextView tvTitle = new TextView(new ContextThemeWrapper(this, R.style.textView));
        tvTitle.setTextSize(13);
        tvTitle.setTypeface(MyMethods.getBoldFont(this));
        tvTitle.setText(getString(R.string.traveler)+ " " + (id + 1));

        TextView tvName = new TextView(new ContextThemeWrapper(this, R.style.textView));
        tvName.setTypeface(font);
        tvName.setText(R.string.full_name_same_passport);
        tvName.setLayoutParams(lyTextView);

        EditText etName = new EditText(new ContextThemeWrapper(this, R.style.editText));
        etName.setTypeface(font);
        etName.setTag("name");
        etName.setHint(getString(R.string.required_english));
        etName.setBackground(getResources().getDrawable(R.drawable.edit_text));
        etName.setLayoutParams(lyEditText);
        etName.setInputType(TYPE_CLASS_TEXT | TYPE_TEXT_FLAG_CAP_CHARACTERS);
        etName.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
//        if (id == 0)
//            etName.setText(setting.ge); //testing remove it later

        TextView tvDob = new TextView(new ContextThemeWrapper(this, R.style.textView));
        tvDob.setTypeface(font);
        tvDob.setText(R.string.date_of_birth);
        tvDob.setLayoutParams(lyTextView);


        //setting dob edittext and button
        FrameLayout flEditText = new FrameLayout(this);
        final EditText etDob = new EditText(new ContextThemeWrapper(this, R.style.editText));
        etDob.setTypeface(font);
        etDob.setTag("dob");
        etDob.setBackground(getResources().getDrawable(R.drawable.edit_text));
        etDob.setEnabled(false);
        ImageView ivDobIcon = new ImageView(this);
        LinearLayout lloDatPicker = new LinearLayout(this);
        int ivDobPadding = getResources().getDimensionPixelOffset(R.dimen.padding_ivDobPicker);
        ivDobIcon.setPadding(ivDobPadding, ivDobPadding, ivDobPadding, ivDobPadding);
        ivDobIcon.setImageResource(R.drawable.ic_date);
        lloDatPicker.setBackground(getResources().getDrawable(R.drawable.button_transparent));
        flEditText.setLayoutParams(lyEditText);
        flEditText.addView(etDob);
        flEditText.addView(ivDobIcon);
        flEditText.addView(lloDatPicker);
        int ivDobHeight = getResources().getDimensionPixelOffset(R.dimen.height_editText);
        ivDobIcon.getLayoutParams().height = ivDobHeight;
        ivDobIcon.getLayoutParams().width = ivDobHeight;
        lloDatPicker.getLayoutParams().height = ivDobHeight;
        lloDatPicker.getLayoutParams().width = ivDobHeight;
        ((FrameLayout.LayoutParams) ivDobIcon.getLayoutParams()).gravity = Gravity.RIGHT;
        ((FrameLayout.LayoutParams) lloDatPicker.getLayoutParams()).gravity = Gravity.RIGHT;

        lloDatPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etDob.requestFocus();
                openPickDateDialog(etDob);
            }
        });
        //etDob.setText("01/01/1987"); //testing remove it later

        TextView tvPassportNo = new TextView(new ContextThemeWrapper(this, R.style.textView));
        tvPassportNo.setTypeface(font);
        tvPassportNo.setText(R.string.passport_no);
        tvPassportNo.setLayoutParams(lyTextView);

        EditText etPassportNo = new EditText(new ContextThemeWrapper(this, R.style.editText));
        etPassportNo.setTypeface(font);
        etPassportNo.setHint(getString(R.string.required_english));
        //etPassportNo.setInputType(InputType.TYPE_CLASS_NUMBER);
        etPassportNo.setTag("passNo");
        etPassportNo.setBackground(getResources().getDrawable(R.drawable.edit_text));
        etPassportNo.setLayoutParams(lyEditText);
        //etPassportNo.setText("123"); //testing remove it later

        TextView tvRelationship = new TextView(new ContextThemeWrapper(this, R.style.textView));
        tvRelationship.setTypeface(font);
        tvRelationship.setText(R.string.relationship);
        tvRelationship.setLayoutParams(lyTextView);

        TextView tvNationalty = new TextView(new ContextThemeWrapper(this , R.style.textView));
        tvNationalty.setTypeface(font);
        tvNationalty.setText("Nationality:");
        tvNationalty.setLayoutParams(lyEditText);


        EditText etNationalty = new EditText(new ContextThemeWrapper(this, R.style.editText));
        etNationalty.setTypeface(font);
        etNationalty.setBackground(getResources().getDrawable(R.drawable.edit_text));
        etNationalty.setTag("Nationality");
        etNationalty.setLayoutParams(lyEditText);
        etNationalty.setInputType(TYPE_CLASS_TEXT| TYPE_TEXT_FLAG_CAP_CHARACTERS);


        Spinner sRelationShip = new Spinner(this, null);
        sRelationShip.setBackground(null);
        sRelationShip.getRootView().setBackground(null);
        sRelationShip.setTag("relation");

        sRelationShip.setLayoutParams(lyEditText);
        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(this, id == 0 ? new String[]{getString(R.string.self)} : getResources().getStringArray(R.array.relationship));
        sRelationShip.setAdapter(spinnerAdapter);

        lloTravelerField.addView(linearLayout);

        linearLayout.addView(tvTitle);
        linearLayout.addView(tvName);
        linearLayout.addView(etName);
        linearLayout.addView(tvDob);
        linearLayout.addView(flEditText);
        linearLayout.addView(tvPassportNo);
        linearLayout.addView(etPassportNo);
        linearLayout.addView(tvRelationship);
        linearLayout.addView(sRelationShip);
        linearLayout.addView(tvNationalty);
        linearLayout.addView(etNationalty);
    }

    private void openPickDateDialog(final EditText editText) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                String mYear, mMonth, mDay;
                mYear = String.valueOf(year);
                mMonth = String.valueOf(monthOfYear + 1);
                mDay = String.valueOf(dayOfMonth);
                String date = mDay + "/" + mMonth + "/" + mYear;
                editText.setText(date);
            }
        }, year, month, day);
        datePickerDialog.show();
    }

    JSONArray membersArray;
    boolean isMissingData = false;
    String cpr;
    private void prepareSaveQuote() {
        membersArray = new JSONArray();
        cpr = etCpr.getText().toString();
        for (int i = 0; i < travelerNo; i++) {
            try {
                JSONObject memberObject = new JSONObject();
                View TravelerView = lloTravelerField.findViewWithTag("traveler" + i);
                String name, dob, passNo, relation , nationality ;
                name = ((EditText) TravelerView.findViewWithTag("name")).getText().toString();
                dob = ((EditText) TravelerView.findViewWithTag("dob")).getText().toString();
                passNo = ((EditText) TravelerView.findViewWithTag("passNo")).getText().toString();
//                relation = ((Spinner) TravelerView.findViewWithTag("relation")).getSelectedItem().toString();
                nationality = ((EditText) TravelerView.findViewWithTag("Nationality")).getText().toString() ;
                //Log.d("nationalitylll", nationality) ;
                relation = getResources().getStringArray(R.array.relationship_value)[((Spinner) TravelerView.findViewWithTag("relation")).getSelectedItemPosition()];
                if (name.isEmpty() || dob.isEmpty() || passNo.isEmpty() || relation.isEmpty() || nationality.isEmpty()) {
                    isMissingData = true;
                    break;
                } else {
                    memberObject.put("name", name);
                    memberObject.put("dob", dob);
                    memberObject.put("passNo", passNo);
                    memberObject.put("relation", relation);
                    memberObject.put("Nationality" , nationality) ;
                    membersArray.put(memberObject);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        Log.i("membersArray", membersArray.toString());

        if (isMissingData || cpr.isEmpty()) {
            Snackbar.make(findViewById(R.id.main), R.string.fill_all_fields,Snackbar.LENGTH_INDEFINITE).
                    setAction(R.string.dialog_button_default, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        }
                    }).show();
        } else {
            saveQuote();
        }
    }

    String Tag_Request = "saveQuote";

    private void saveQuote() {
        final ProgressDialog mDialog = new ProgressDialog(this);
        mDialog.setMessage(getString(R.string.progress_loading));
        mDialog.show();
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetSaveTravelQuote
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mDialog.dismiss();
                getSaveQuoteResult(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mDialog.dismiss();
                Snackbar.make(findViewById(R.id.main), R.string.networkError, Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                saveQuote();
                            }
                        }).show();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                Intent i = getIntent();
                params.put("premium", String.valueOf(premium));
                params.put("policyStartDate", i.getStringExtra("policyStartDate"));
                params.put("area", i.getStringExtra("area"));
                params.put("planType", i.getStringExtra("planType"));
                params.put("period", i.getStringExtra("period"));
                params.put("depDate", i.getStringExtra("depDate"));
                params.put("destination", i.getStringExtra("destination"));
                params.put("members", membersArray.toString());
                params.put("cpr", cpr);
                params.put("saveQuote", "1");
                params.put("userId", String.valueOf(setting.getUserId()));
                params.put("token", setting.getAccessToken());
                params.put("deviceId", MyMethods.getDeviceId(SaveQuoteTravelActivity.this));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void getSaveQuoteResult(final JSONObject jsonObject) {
        try {
            Log.i("executeResult", jsonObject.toString());
            if (jsonObject.getBoolean("success")) {
                String quotationNo = jsonObject.getString("result");
                Log.d("quoteno" , quotationNo ) ;

                Intent intent = new Intent(this, PaymentActivity.class);
                intent.putExtra("premium", premium);
                intent.putExtra("no", quotationNo);
                intent.putExtra("payFor", "new");
                startActivity(intent);
            } else if (jsonObject.getInt("status") == 4) {
                MyMethods.showLoginPage(this);
            } else if (jsonObject.getInt("status") == 3) {
                MyMethods.showNetworkErrorDialog(SaveQuoteTravelActivity.this,
                        getString(R.string.message_quoteSaveFailed), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                try {
                                    retryApproveQuote(jsonObject.getString("result"));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
            } else {
                Snackbar.make(findViewById(R.id.main), R.string.networkError,Snackbar.LENGTH_INDEFINITE).
                        setAction(R.string.dialog_button_default, new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                            }
                        }).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    //The below request should occurred in the saveQuote request but if it failed we request it alone
    String Tag_Request_Approve = "approveQuote";

    private void retryApproveQuote(final String quotationNo) {
        final ProgressDialog mDialog = new ProgressDialog(this);
        mDialog.setMessage(getString(R.string.progress_loading));
        mDialog.show();
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.ApproveQuote
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                mDialog.dismiss();
                getSaveQuoteResult(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                mDialog.dismiss();
                //We did not use SnackBar because the user must retry to complete the process perfectly
                MyMethods.showNetworkErrorDialog(SaveQuoteTravelActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        retryApproveQuote(quotationNo);
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                params.put("quotationNo", quotationNo);
                params.put("userId", String.valueOf(setting.getUserId()));
                params.put("token", setting.getAccessToken());
                params.put("cpr", setting.getCpr());
                params.put("deviceId", MyMethods.getDeviceId(SaveQuoteTravelActivity.this));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request_Approve);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.confirm, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.confirm:
                MyMethods.hideKeyboard(this);
                prepareSaveQuote();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
