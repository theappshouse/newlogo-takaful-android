package com.appshouse.takaful.activities;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.appshouse.takaful.utilities.LruBitmapCache;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.SharedPreferencesClass;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.splunk.mint.Mint;


/**
 * Created by Mohammed Algassab on 11/6/2014.
 */
public class AppController extends Application {
    public static final String TAG = AppController.class.getSimpleName();
    private static AppController mInstance;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;
    SharedPreferencesClass setting;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        setting = new SharedPreferencesClass(this);
        Mint.initAndStartSession(this, "6bb38b92");
        //The parse for now will be used only to get notification from server about the motor if it is approved

        //testing
//        Parse.initialize(new Parse.Configuration.Builder(this)
//                .applicationId("elWzVnAyyjPtrboBlzlm64DMQ4GEjms12oV3exvC")
//                .clientKey("kFRs2B7TCR1xKwmnWwdis4dsgK7XWLF7ssAuDhLJ")
//                .server("https://pg-app-094jqo61ilnerhvrjdkchyd7ixd8se.scalabl.cloud/1/").build());

        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId("xwD83pmhBtrGLmerPjHlKX5Y815iBOpGBYLPOY69")
                .clientKey("v49AeKFS9BfjEjIboTERvefT3w7JrGy1V4fKeQE0")
                .server("https://pg-app-wg22nv5apr6v3ec2kk9lxtfedugh2w.scalabl.cloud/1/").build());

        ParseInstallation.getCurrentInstallation().saveInBackground();
        ParsePush.subscribeInBackground("true");
        Parse.setLogLevel(Parse.LOG_LEVEL_VERBOSE);
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();

        //testing
//        installation.put("GCMSenderId", "568566837366");

        installation.put("GCMSenderId", "382564968900");
        installation.saveInBackground();
        //MyMethods.changeLanguage(this, setting.getLanguage());
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.mRequestQueue, new LruBitmapCache());
        }
        return this.mImageLoader;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
