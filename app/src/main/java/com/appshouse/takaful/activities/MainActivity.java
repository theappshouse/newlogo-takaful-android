package com.appshouse.takaful.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Request;
import com.android.volley.Response;
import com.appshouse.takaful.R;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.fragments.ClaimListFragment;
import com.appshouse.takaful.fragments.HomeFragment;
import com.appshouse.takaful.fragments.LoginFragment;
import com.appshouse.takaful.fragments.MyTakafulFragment;
import com.appshouse.takaful.fragments.UserInfoFragment;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.SharedPreferencesClass;
import com.splunk.mint.Mint;
import com.splunk.mint.MintLogLevel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {
    Toolbar toolbar;
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    Fragment currentFragment;
    boolean isHomeFragment = true;
    SharedPreferencesClass setting;
    MenuItem miLogin, miAddClaim, miLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setting = new SharedPreferencesClass(this);
        MyMethods.changeLanguage(this, setting.getLanguage());
        setContentView(R.layout.activity_main);

        //Testing splunk
        HashMap<String, Object> mydata = new HashMap<>();
        mydata.put("time", System.currentTimeMillis());
        Mint.logEvent("Open App", MintLogLevel.Info, mydata);


        //Create and set the action bar
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.slideMenuOpen, R.string.slideMenuClose);
        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        drawerLayout.post(new Runnable() {
            @Override
            public void run() {
                actionBarDrawerToggle.syncState();
            }
        });
        drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        ViewGroup root = (ViewGroup) findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(this));



        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                findViewById(R.id.ivSplash).setVisibility(View.GONE);

                if (getIntent().getBooleanExtra("isLoginPage", false))
                    replaceFragment(new LoginFragment(), getString(R.string.sm_login), false);
                else if (getIntent().getBooleanExtra("isMyTakafulPage", false))
                    replaceFragment(new MyTakafulFragment(), getString(R.string.sm_myTakaful), false);
                else if (getIntent().getBooleanExtra("isClaimPage", false))
                    replaceFragment(new ClaimListFragment(), getString(R.string.sm_myClaim), false);
                else
                    replaceFragment(new HomeFragment(), getString(R.string.sm_home), true);
            }
        }, 2500);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkForUpdate();
    }

    String Tag_Request = "checkUpdate";

    private void checkForUpdate() {
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.CheckUpdate
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                setCheckForUpdate(response);
            }
        },null);
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void setCheckForUpdate(JSONObject response) {
        try {
            Log.i("setCheckForUpdate", response.toString());
            if (response.getBoolean("success")) {
                JSONArray resultArray = response.getJSONArray("result");
                if(resultArray.length() > 0)
                {
                    JSONObject jsonObject = resultArray.getJSONObject(0);
                    int versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
                    if (jsonObject.getInt("Critical_Update_No") > versionCode) {
                        showMajorUpdateDialog();
                    } else if (jsonObject.getInt("Minor_Update_No") > versionCode) {
                        showMinorUpdateDialog();
                    } else {
                        Log.i("setCheckForUpdate", "No update");
                    }
                }

            } else {
                MyMethods.showNetworkErrorDialog(MainActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        checkForUpdate();
                    }
                });
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void showMajorUpdateDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(R.string.dialog_title_majorUpdate);
        dialog.setMessage(R.string.dialog_message_majorUpdate);
        dialog.setPositiveButton(R.string.dialog_button_update, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                MyMethods.openLink(MainActivity.this, getString(R.string.play_store_link));
            }
        });
        dialog.setNegativeButton(R.string.dialog_button_closeApp, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }

    private void showMinorUpdateDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(R.string.dialog_title_minorUpdate);
        dialog.setMessage(R.string.dialog_message_minorUpdate);
        dialog.setPositiveButton(R.string.dialog_button_update, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                MyMethods.openLink(MainActivity.this, getString(R.string.play_store_link));
            }
        });
        dialog.setNegativeButton(R.string.dialog_button_cancel, null);
        dialog.show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //No call for super(). Bug on API Level > 11.
    }

    public void replaceFragment(Fragment fragment, String title, boolean isHomeFragment) {
        Log.i("replaceFragment", title);
        if (miLogin != null) {
            miLogin.setVisible(isHomeFragment);
            //todo: uncomiit it after translation publ
//            miLanguage.setVisible(isHomeFragment);
        }
        this.isHomeFragment = isHomeFragment;
        currentFragment = fragment;
        getSupportFragmentManager().beginTransaction().replace(R.id.flMain, currentFragment)
                .disallowAddToBackStack().commitAllowingStateLoss();
        toolbar.setTitle(title);
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {

    }

    //  public void loginLogout() {
//      ((SideMenuFragment) getFragmentManager().findFragmentById(R.id.slideMenu)).resetSlideMenu();
//  }

    public void closeDrawerLayout() {
        drawerLayout.closeDrawers();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        miLogin = menu.findItem(R.id.login);
        miAddClaim = menu.findItem(R.id.addClaim);
        //todo: uncomiit it after translation publ
//        miLanguage = menu.findItem(R.id.language);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.login:
                if (setting.isLogin()) {
                    replaceFragment(new UserInfoFragment(), getString(R.string.sm_userInfo), false);
                } else {
                    replaceFragment(new LoginFragment(), getString(R.string.sm_login), false);
                }
                break;
            case R.id.addClaim:
                startActivity(new Intent(this, AddMotorClaimActivity.class));
                break;
            //todo: uncomiit it after translation publ
            case R.id.language:
                setting.changeLanguage(this,setting.getLanguage().contentEquals("en") ? "ar" : "en");
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(Gravity.LEFT))
            drawerLayout.closeDrawers();
        else if (isHomeFragment) {
            finish();
        } else {
            replaceFragment(new HomeFragment(), getString(R.string.sm_home), true);
        }

    }

    public void setAddClaimMenu(boolean isClaim) {
        if (miAddClaim != null)
            miAddClaim.setVisible(isClaim);
    }
}
