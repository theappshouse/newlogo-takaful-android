package com.appshouse.takaful.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.appshouse.takaful.R;
import com.appshouse.takaful.attributes.News;
import com.appshouse.takaful.constant.APIs;
import com.appshouse.takaful.utilities.CustomJSONObject;
import com.appshouse.takaful.utilities.MyMethods;
import com.appshouse.takaful.utilities.MyTagHandler;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mohammed Algassab on 2/6/2016.
 */
public class NewsDetailActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView tvNewsTitle, tvNewsDate, tvNewsDetail;
    ImageView ivNews;
    News news;
    View view;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);
        view = findViewById(android.R.id.content);
        toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.title_newsDetail);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ViewGroup root = (ViewGroup) findViewById(R.id.main);
        MyMethods.setFont(root, MyMethods.getRegularFont(this));

        tvNewsTitle = (TextView) findViewById(R.id.tvNewsTitle);
        tvNewsDate = (TextView) findViewById(R.id.tvNewsDate);
        tvNewsDetail = (TextView) findViewById(R.id.tvNewsDetail);
        ivNews = (ImageView) findViewById(R.id.ivNews);

        if (getIntent().getExtras().containsKey("news")) {
            news = getIntent().getParcelableExtra("news");
            setNewsDetail();
        } else {
            getNewsDetail(getIntent().getIntExtra("newsId", 0));
        }
    }

    String Tag_Request = "getNewsDetail";

    private void getNewsDetail(final int newsId) {
        MyMethods.showLoading(view);
        CustomJSONObject req = new CustomJSONObject(Request.Method.POST, APIs.GetNewsDetail
                , null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                MyMethods.showMainView(view);
                try {
                    JSONObject newsObject = response.getJSONObject("result");
                    news = new News(newsObject.getInt("Id"), newsObject.getString("News_Title"),
                            newsObject.getString("News_Details"), newsObject.getString("News_Date"),
                            newsObject.getString("News_Image"));

                    setNewsDetail();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                MyMethods.showNetworkErrorDialog(NewsDetailActivity.this, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getNewsDetail(newsId);
                    }
                });
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<>();
                Log.i("getNewsDetail", " newsId:" + newsId);
                params.put("newsId", String.valueOf(newsId));
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue(req, Tag_Request);
    }

    private void setNewsDetail() {
        Picasso.with(this).load(APIs.ImageURL + news.mImage.replaceAll(" ", "%20")).placeholder(R.drawable.news_sample).into(ivNews);
        tvNewsTitle.setText(news.mTitle);
        tvNewsDate.setText(news.mDate);
        tvNewsDetail.setText(Html.fromHtml(news.mDesc,null,new MyTagHandler()));
    }
}
